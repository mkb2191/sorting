def quicksort(data, start=0, end=None, max_num=None):
    if end==None:
        end=len(data) -1
    to_sort = [(start, end)]
    while to_sort:
        start, end = to_sort[-1]
        if end - start > 0:
            pivot_pos = end
            pivot = data[pivot_pos]
            appended = False
            for first in range(start, end):
                found = False
                if pivot < data[first]:
                    for second in range(end -1, first, -1):
                        if data[second] < pivot:
                            data[first], data[second] = data[second], data[first]
                            found = True
                            break
                    if not found:
                        data[first], data[pivot_pos] = data[pivot_pos], data[first]
                        to_sort[-1] = (start, first -1)
                        to_sort.append((first + 1, end))
                        appended = True
                        break
            if not appended:
                to_sort[-1] = (start, end-1)
        else:
            del to_sort[-1]
