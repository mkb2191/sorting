def limited_unique_positive(data, start=0, end=None, max_num=10):
    if end==None:
        end=len(data) -1
    options = list(bytes(max_num))
    for i in data:
        options[i] += 1
    pos = 0
    for i, count in enumerate(options):
        data[pos:pos+count] = (i,) * count
        pos += count
    return data
