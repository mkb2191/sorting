def insertion_sort(data, start=0, end=None, max_num=None):
    if end==None:
        end=len(data) -1
    for position in range(start + 1, end + 1):
        to_insert = data[position]
        for comp in range(position):
            if data[comp] > to_insert:
                del data[position]
                data.insert(comp, to_insert)
                break
