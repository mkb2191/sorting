def selection_sort_double_ended(data, start=0, end=None, max_num=None):
    if end==None:
        end=len(data) -1
    for pos_low_0 in range(start, int(end/2)):
        low = data[pos_low_0]
        pos_low_1 = None
        pos_high_0 = end - pos_low_0
        high = data[pos_high_0]
        pos_high_1 = None
        for i in range(pos_low_0, pos_high_0 + 1):
            item = data[i]
            if item > high:
                high = item
                pos_high_1 = i
            if item < low:
                low = item
                pos_low_1 = i
        if pos_low_1 is not None:
            if pos_high_1 is not None:
                if pos_high_1 == pos_low_0:
                    data[pos_low_0], data[pos_low_1], data[pos_high_0] = data[pos_low_1], data[pos_high_0], data[pos_low_0]
                elif pos_high_0 == pos_low_1:
                    data[pos_low_0], data[pos_low_1], data[pos_high_1] = data[pos_low_1], data[pos_high_1], data[pos_low_0]
                else:
                    data[pos_low_0], data[pos_low_1] = data[pos_low_1], data[pos_low_0]
                    data[pos_high_0], data[pos_high_1] = data[pos_high_1], data[pos_high_0]
            else:
                data[pos_low_0], data[pos_low_1] = data[pos_low_1], data[pos_low_0]
        elif pos_high_1 is not None:
            data[pos_high_0], data[pos_high_1] = data[pos_high_1], data[pos_high_0]
