from limited_unique_positive import limited_unique_positive
from quicksort import quicksort
from insertion import insertion_sort
from selection_double import selection_sort_double_ended
from selection import selection_sort

import random
import time
base = []
for i in range(10001):
    base.append(random.randint(0, 1000000))
print('Generated list')
for method, name in zip([limited_unique_positive, quicksort, insertion_sort, selection_sort_double_ended, selection_sort],
                        ['limited_unique_positive', 'quick', 'insertion', 'selection_sort_double_ended', 'selection', ]):
    data = base.copy()
    start= time.time()
    method(data, max_num=1000001)
    print(name, 'Correctly sorted:', data==sorted(data), 'in %ss' % (time.time() - start))
