def selection_sort(data, start=0, end=None, max_num=None):
    if end==None:
        end=len(data) -1
    for pos in range(start, end):
        smallest = data[pos]
        new_pos = None
        for i in range(pos+1, end+1):
            item = data[i]
            if item < smallest:
                smallest = item
                new_pos = i
        if new_pos is not None:
            data[pos], data[new_pos] = data[new_pos], data[pos]
